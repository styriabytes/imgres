# Imgres

Imgres is an image seo optimization tool.

**Usage:**
```
imres -target="original;16:9;300x300,600x600" image.jpg
imres -target="thumbnail;golden;300x300,600x600" image.jpg
imres -target="square;square;300x300,600x600" image.jpg
imres -target="cropped;30,30,370,370;300x300,600x600" image.jpg
```


## Contribution

Any type of feedback, pull request or issue is welcome.

## Licence

Imgres is licenced under the MIT licence.