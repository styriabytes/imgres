package main

import (
	"flag"
	"fmt"
	"github.com/disintegration/imaging"
	"image"
	"math"
	"os"
	"path"
	"strconv"
	"strings"
)

const processDelimiter = ";"
const cropRectangleDelimiter = ","
const sizesDelimiter = ","
const aspectRatioDelimiter = ":"

const goldenRatio = 1.618

type arrayFlags []string

func (i *arrayFlags) String() string {
	return "my string representation"
}
func (i *arrayFlags) Set(val string) error {
	*i = append(*i, val)
	return nil
}

// INFO: https://play.golang.org/p/XJTojeDaBHV
type imageProcessor struct {
	namespace string
	crop      CroppingData
	sizes     []picSize
}

func (i *imageProcessor) Save(src image.Image, filename string) error {
	if i.HasNamespace() {
		filename = path.Join(path.Dir(filename), i.namespace, path.Base(filename))
		err := os.MkdirAll(path.Dir(filename), 0755)
		if err != nil {
			return fmt.Errorf("Cannot crate filepath: %w\n", err)
		}
	}
	return imaging.Save(src, filename)
}
func (i *imageProcessor) HasNamespace() bool {
	return i.namespace != ""
}
func (i *imageProcessor) HasCroppingData() bool {
	return i.crop.HasAction()
}
func (i *imageProcessor) HasSizes() bool {
	return len(i.sizes) > 0
}

type CroppingData struct {
	action string
	ratio  float64
	rect   image.Rectangle
}

func (c *CroppingData) HasAction() bool {
	return c.action != ""
}
func (c *CroppingData) IsActionCrop() bool {
	return c.action == "crop"
}
func (c *CroppingData) IsActiobRatio() bool {
	return c.action == "ratio"
}
func (c *CroppingData) HasRectangle() bool {
	// TODO(ssandriesser): benchmark with i.crop != image.Rectangle{}
	return !(c.rect.Max.X == 0 && c.rect.Max.Y == 0 && c.rect.Min.X == 0 && c.rect.Min.Y == 0)
}

type picSize struct {
	width  int
	height int
}

var (
	flagModifiers arrayFlags
)

func main() {
	flag.Var(&flagModifiers, "process", "list of modifiers")
	flag.Parse()

	files := flag.Args()
	if len(files) != 1 {
		fmt.Println("Wrong count of arguments")
		os.Exit(1)
	}

	imgProcessors, err := parseProcesses(flagModifiers)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// check if file exists
	srcOrig, err := imaging.Open(files[0])
	if err != nil {
		fmt.Println("Cannot open image: ", err)
		os.Exit(1)
	}
	filename := files[0]

	for _, imgProcessor := range imgProcessors {
		src := srcOrig

		if imgProcessor.crop.IsActionCrop() {
			src = imaging.Crop(src, imgProcessor.crop.rect)
		} else if imgProcessor.crop.IsActiobRatio() {
			bounds := src.Bounds()
			w, h, _ := fromRatio(imgProcessor.crop.ratio, int64(bounds.Max.X), int64(bounds.Max.Y))
			src = imaging.Thumbnail(src, int(w), int(h), imaging.Lanczos)
		}

		err = imgProcessor.Save(src, filename)
		if err != nil {
			fmt.Println("Error save file: ", err)
			os.Exit(1)
		}

		if !imgProcessor.HasSizes() {
			continue
		}
		for _, size := range imgProcessor.sizes {
			srcResized := imaging.Fit(src, size.width, size.height, imaging.Lanczos)
			err = imgProcessor.Save(srcResized, appendFilename(filename, "_w"+fmt.Sprint(size.width)))
			if err != nil {
				fmt.Println("Error save file: ", err)
				os.Exit(1)
			}
		}
	}
}

func appendFilename(filename, append string) string {
	fExt := path.Ext(filename)
	fname := strings.TrimSuffix(path.Base(filename), fExt)
	return path.Join(path.Dir(filename), fname+append+fExt)
}

func parseSize(s string) (picSize, error) {
	var ps picSize

	parts := strings.Split(s, "x")
	if len(parts) != 2 {
		return ps, fmt.Errorf("%s has not the correct form", s)
	}

	var err error
	ps.width, err = strconv.Atoi(strings.TrimSpace(parts[0]))
	if err != nil {
		return picSize{}, fmt.Errorf("parseSize: ps.x: %w", err)
	}

	ps.height, err = strconv.Atoi(strings.TrimSpace(parts[1]))
	if err != nil {
		return picSize{}, fmt.Errorf("parseSize: ps.y: %w", err)
	}

	return ps, nil
}

func min(x, y int64) int64 {
	if x < y {
		return x
	}
	return y
}

func parseAspectRatio(ratio string, reciprocal bool) (float64, error) {
	switch ratio {
	case "golden":
		if reciprocal {
			return 1 / goldenRatio, nil
		}
		return goldenRatio, nil
	case "square", "1:1":
		return 1, nil
	case "2:1":
		if reciprocal {
			return 0.5, nil
		}
		return 2, nil
	}
	values := strings.Split(ratio, aspectRatioDelimiter)
	if len(values) != 2 {
		return 0, fmt.Errorf("parseAspectRatio: can't parse ratio: %s", ratio)
	}
	valA, err := strconv.ParseFloat(values[0], 64)
	if err != nil {
		return 0, fmt.Errorf("parseAspectRatio: can't parse ratio: %s", ratio)
	}
	valB, err := strconv.ParseFloat(values[1], 64)
	if err != nil {
		return 0, fmt.Errorf("parseAspectRatio: can't parse ratio: %s", ratio)
	}
	if reciprocal {
		return valB / valA, nil
	}
	return valA / valB, nil
}

func fromRatio(ratio float64, w, h int64) (int64, int64, error) {
	var wr int64
	var hr int64

	switch ratio {
	case 1:
		wr = min(w, h)
		hr = wr
		return wr, hr, nil
	}
	wr = w
	hr = int64(math.Floor(float64(w) / ratio))
	if hr > h {
		wr = int64(math.Floor(float64(h) * ratio))
		hr = h
	}
	return wr, hr, nil
}

func parseProcesses(processes []string) ([]imageProcessor, error) {
	imgProcessors := []imageProcessor{}
	for _, p := range processes {
		imgProcessor, err := parseProcess(strings.TrimSpace(p))
		if err != nil {
			return []imageProcessor{}, fmt.Errorf("parseProcesses: %w", err)
		}
		imgProcessors = append(imgProcessors, imgProcessor)
	}
	return imgProcessors, nil
}

func parseProcess(val string) (imageProcessor, error) {
	data := strings.Split(val, processDelimiter)
	if len(data) != 3 {
		return imageProcessor{}, fmt.Errorf("process value incompatible\n")
	}
	p := imageProcessor{}
	p.namespace = data[0]

	croppingData, err := parseCropping(data[1])
	if err != nil {
		return imageProcessor{}, fmt.Errorf("parseProcess: %w", err)
	}
	p.crop = croppingData

	var pSizes []picSize
	pSizes, err = parseSizes(data[2])
	if err != nil {
		return imageProcessor{}, fmt.Errorf("parseProcess: %w", err)
	}
	p.sizes = pSizes

	return p, nil
}

func parseCropping(val string) (CroppingData, error) {
	croppingData := CroppingData{}

	if val == "" {
		return croppingData, nil
	}

	points := strings.Split(val, cropRectangleDelimiter)

	if len(points) == 1 {
		ratio, err := parseAspectRatio(points[0], false)
		if err != nil {
			return CroppingData{}, fmt.Errorf("parseCropping: %w", err)
		}
		croppingData.action = "ratio"
		croppingData.ratio = ratio
		return croppingData, nil
	}

	if len(points) != 4 {
		return croppingData, fmt.Errorf("parseCropping: wrong count of points")
	}

	var err error

	var x0 int
	x0, err = strconv.Atoi(points[0])
	if err != nil {
		return croppingData, fmt.Errorf("parseCropping: x0: %w", err)
	}

	var y0 int
	y0, err = strconv.Atoi(points[1])
	if err != nil {
		return croppingData, fmt.Errorf("parseCropping: y0: %w", err)
	}

	var x1 int
	x1, err = strconv.Atoi(points[2])
	if err != nil {
		return croppingData, fmt.Errorf("parseCropping: x1: %w", err)
	}

	var y1 int
	y1, err = strconv.Atoi(points[3])
	if err != nil {
		return croppingData, fmt.Errorf("parseCropping: y1: %w", err)
	}
	croppingData.action = "crop"
	croppingData.rect = image.Rect(x0, y0, x1, y1)
	return croppingData, nil
}

func parseSizes(val string) ([]picSize, error) {
	pSizes := []picSize{}

	if val == "" {
		return pSizes, nil
	}

	sizes := strings.Split(val, sizesDelimiter)
	for _, size := range sizes {
		pSize, err := parseSize(size)
		if err != nil {
			return []picSize{}, fmt.Errorf("parseSizes: %w", err)
		}
		pSizes = append(pSizes, pSize)
	}
	return pSizes, nil
}
