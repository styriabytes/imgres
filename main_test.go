package main

import (
	"fmt"
	"image"
	"reflect"
	"testing"
)

func TestAppendFilename(t *testing.T) {
	tests := []struct {
		filename string
		append   string
		want     string
	}{
		{"/var/www/page/storage/test.jpg", "", "/var/www/page/storage/test.jpg"},
		{"/var/www/page/storage/test.jpg", "_123", "/var/www/page/storage/test_123.jpg"},
		{"test.jpg", "_123", "test_123.jpg"},
		{"./test.jpg", "_123", "test_123.jpg"},
		{"../test.jpg", "_123", "../test_123.jpg"},
	}
	for _, tt := range tests {
		t.Run(tt.filename, func(t *testing.T) {
			got := appendFilename(tt.filename, tt.append)
			if got != tt.want {
				t.Errorf("appendFilename(%s, %s)=%v want %v", tt.filename, tt.append, got, tt.want)
			}
		})
	}
}

func TestParseSize(t *testing.T) {
	tests := []struct {
		s       string
		want    picSize
		wantErr bool
	}{
		{"500x500", picSize{500, 500}, false},
		{" 500x500 ", picSize{500, 500}, false},
		{"500-500", picSize{}, true},
		{"", picSize{}, true},
		{"x", picSize{}, true},
		{"500", picSize{}, true},
		{"500x", picSize{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			got, err := parseSize(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseSize(%s) error = %v, wantErr %v", tt.s, err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseSize(%s)=%v want %v", tt.s, got, tt.want)
			}
		})
	}
}

func TestParseProcess(t *testing.T) {
	tests := []struct {
		input   string
		want    imageProcessor
		wantErr bool
	}{
		// Positives
		{";;", imageProcessor{sizes: []picSize{}}, false},
		{"thumbnail;;", imageProcessor{namespace: "thumbnail", sizes: []picSize{}}, false},
		{"thumbnail;10,10,30,30;", imageProcessor{namespace: "thumbnail", crop: CroppingData{action: "crop", rect: image.Rectangle{Min: image.Point{X: 10, Y: 10}, Max: image.Point{X: 30, Y: 30}}}, sizes: []picSize{}}, false},
		{";;300x300,600x600", imageProcessor{sizes: []picSize{{300, 300}, {600, 600}}}, false},

		// Negatives
		{"", imageProcessor{}, true},
		{"thumbnail;", imageProcessor{}, true},
		{";;;", imageProcessor{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.input, func(t *testing.T) {
			got, err := parseProcess(tt.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseProcess(%s) error = %v, wantErr %v", tt.input, err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseProcess(%s)=%v want %v", tt.input, got, tt.want)
			}
		})
	}
}

func TestParseCropping(t *testing.T) {
	tests := []struct {
		input      string
		want       CroppingData
		wantAction string
		wantErr    bool
	}{
		// Positives
		{"", CroppingData{}, "", false},
		{"10,20,30,40", CroppingData{action: "crop", rect: image.Rectangle{Min: image.Point{X: 10, Y: 20}, Max: image.Point{X: 30, Y: 40}}}, "crop", false},
		{"4:3", CroppingData{action: "ratio", ratio: 4.0 / 3.0}, "ratio", false},
		{"square", CroppingData{action: "ratio", ratio: 1}, "ratio", false},

		// Negatives
		{"10", CroppingData{}, "", true},
		{"10,20", CroppingData{}, "", true},
		{"10,20,30", CroppingData{}, "", true},
		{"10,20,30,40,50", CroppingData{}, "", true},
		{"10,20,30,40.1", CroppingData{}, "", true},
		{"10,20,a,40", CroppingData{}, "", true},
		{"10,20,,40", CroppingData{}, "", true},
		{",,,", CroppingData{}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.input, func(t *testing.T) {
			got, err := parseCropping(tt.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseCropping(%s) error = %v, wantErr %v", tt.input, err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseCropping(%s)=%v want %v", tt.input, got, tt.want)
			}
		})
	}
}

func TestParseSizes(t *testing.T) {
	tests := []struct {
		s       string
		want    []picSize
		wantErr bool
	}{
		// Positives
		{"", []picSize{}, false},
		{"300x300", []picSize{{300, 300}}, false},
		{"300x300, 600x600", []picSize{{300, 300}, {600, 600}}, false},

		// Negatives
		{",", []picSize{}, true},
		{"300x300,", []picSize{}, true},
	}
	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			got, err := parseSizes(tt.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseSizes(%s) error = %v, wantErr %v", tt.s, err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseSizes(%s)=%v want %v", tt.s, got, tt.want)
			}
		})
	}
}

func TestFromRatio(t *testing.T) {
	tests := []struct {
		ratio      float64
		width      int64
		height     int64
		wantWidth  int64
		wantHeight int64
		wantErr    bool
	}{
		{1, 300, 150, 150, 150, false},
		{goldenRatio, 300, 450, 300, 185, false},
		{goldenRatio, 300, 160, 258, 160, false},
		{4.0 / 3.0, 333, 123, 164, 123, false},
		{16.0 / 9.0, 1280, 1280, 1280, 720, false},
	}
	for _, tt := range tests {
		txt := fmt.Sprint(tt.ratio) + ":1 - " + fmt.Sprint(tt.width) + "x" + fmt.Sprint(tt.height)
		t.Run(txt, func(t *testing.T) {
			gotWidth, gotHeight, err := fromRatio(tt.ratio, tt.width, tt.height)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseSizes(%f, %d, %d) error = %v, wantErr %v", tt.ratio, tt.width, tt.height, err, tt.wantErr)
				return
			}
			if !(gotWidth == tt.wantWidth && gotHeight == tt.wantHeight) {
				t.Errorf("fromRatio(%f, %d, %d)=%v,%v want %v,%v", tt.ratio, tt.width, tt.height, gotWidth, gotHeight, tt.wantWidth, tt.wantHeight)
			}
		})
	}
}

func TestParseAspectRatio(t *testing.T) {
	tests := []struct {
		ratio      string
		reciprocal bool
		want       float64
		wantErr    bool
	}{
		// Positives
		{"1:1", false, 1, false},
		{"2:1", false, 2, false},
		{"3:2", false, 3.0 / 2.0, false},
		{"3.5:2", false, 3.5 / 2.0, false},
		{"16:9", false, 16.0 / 9.0, false},
		{"golden", false, goldenRatio, false},

		{"1:1", true, 1, false},
		{"2:1", true, 0.5, false},
		{"3:2", true, 2.0 / 3.0, false},
		{"3.5:2", true, 2.0 / 3.5, false},
		{"16:9", true, 9.0 / 16.0, false},
		{"golden", true, 1.0 / goldenRatio, false},

		// Negatives
		{"", false, 0, true},
		{":", false, 0, true},
		{"1:2:3", false, 0, true},
		{"1:", false, 0, true},
		{":2", false, 0, true},
		{"a:2", false, 0, true},
	}
	for _, tt := range tests {
		t.Run(tt.ratio, func(t *testing.T) {
			got, err := parseAspectRatio(tt.ratio, tt.reciprocal)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseAspectRatio(%s, %v) error = %v, wantErr %v", tt.ratio, tt.reciprocal, err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("parseAspectRatio(%s, %v)=%v want %v", tt.ratio, tt.reciprocal, got, tt.want)
			}
		})
	}
}
